RouterOS Backup script
======================

Bash scripts for back-up of MikroTik RouterOS devices configuration.


Usage
-----
```bash
echo 'Exporting MyRouter...'
bakRouterOS 'ssh 10.0.0.1' > my_router.rsc

echo 'Exporting MyGW...'
bakRouterOS 'ssh admin@192.168.63.2 -p 4321' > my_gw.rsc

echo 'Exporting MyOtherRouter...'
bakRouterOS 'ssh -o PubkeyAcceptedKeyTypes=+ssh-rsa 123.145.167.189 -p 12345' > my_other_router.rsc
```

Since the `bakRouterOS` script was rewritten to output the export into stdout
(as that allows piping it into another filters, etc.),
there is no automatic echo of the `Exporting ... filename` line.
If You want it back, a simple function might be enough:
```bash
function echoSave {
	echo "Exporting... $1"
	cat > "$1"
}


bakRouterOS 'ssh 10.0.0.1' |echoSave my_router.rsc

bakRouterOS 'ssh admin@192.168.63.2 -p 4321' |echoSave my_gw.rsc

bakRouterOS 'ssh -o PubkeyAcceptedKeyTypes=+ssh-rsa 123.145.167.189 -p 12345' \
	|echoSave my_other_router.rsc
```

### RB4011
When exporting the configuration from RB4011 devices, there are lines like
`/interface ethernet switch port set 0 default-vlan-id=0`.
They are usually there, even when the router is reset into default config.

To remove them, pipe the export through `cleanRB4011` script:
```bash
echo 'Exporting MyRB4011...'
bakRouterOS 'ssh admin@192.168.63.2 -p 4321' \
	|cleanRB4011 \
	> my_rb4011.rsc
```


Author
------
Elektro-potkan <git@elektro-potkan.cz>


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).


License
-------
This program is licensed under the MIT License.

See file [LICENSE](LICENSE).
